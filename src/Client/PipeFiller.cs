using System.IO.Pipelines;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace TcpEcho {
	public class PipeFiller {
		private const int MinimumBufferSize = 1024;

		private readonly Socket _socket;
		private readonly PipeWriter _writer;

		public PipeFiller(Socket socket, PipeWriter writer) {
			_writer = writer;
			_socket = socket;
		}
		
		private async Task GetDataFromSocket() {
			var memory = _writer.GetMemory(MinimumBufferSize);
			var bytesRead = await _socket.ReceiveAsync(memory, SocketFlags.None);
			_writer.Advance(bytesRead);
		}

		public async Task FillPipeAsync () {
			await Task.Yield();
			while (true)
			{
				if (_socket.Available > 0) {
					await GetDataFromSocket();
				}

				var result = await _writer.FlushAsync();
				if (result.IsCompleted)
				{
					break;
				}
			}

			_writer.Complete();
		}
	}
}