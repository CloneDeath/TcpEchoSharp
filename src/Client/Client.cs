using System;
using System.Buffers;
using System.IO.Pipelines;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;


namespace TcpEcho {

    public class CommunicationUnsuccessfulException : Exception
    {
        internal CommunicationUnsuccessfulException(string msg, Exception innerException) : base(msg, innerException)
        {
        }
    }

    public abstract class Client {
        private static readonly TimeSpan TcpTimeout = TimeSpan.FromSeconds(1);
        private readonly string _endpoint;
        private readonly int _port;

        protected Client (string endpoint, int port) {
            _endpoint = endpoint;
            _port = port;
        }

        private async Task<string> CallImpl (string json) {
            using (Socket socket = new Socket (SocketType.Stream, ProtocolType.Tcp)) {
                socket.ReceiveTimeout = (int)TcpTimeout.TotalMilliseconds;

                var connectTimeOut = Task.Delay (TcpTimeout);
                var completedConnTask = await Task.WhenAny (connectTimeOut, socket.ConnectAsync (_endpoint, _port));
                if (completedConnTask == connectTimeOut) {
                    throw new TimeoutException("connect timed out");
                }

                var bytesToSend = Encoding.UTF8.GetBytes (json + Environment.NewLine);
                socket.Send (bytesToSend);

                var pipe = new Pipe ();
                var writing = new PipeFiller(socket, pipe.Writer).FillPipeAsync();
                var reading = ReadFromPipeAsync (pipe.Reader);

                var readAndWriteTask = Task.WhenAll (reading, writing);
                var readTimeOut = Task.Delay (TcpTimeout);
                var completedReadTask = await Task.WhenAny (readTimeOut, readAndWriteTask);
                if (completedReadTask == readTimeOut) {
                    throw new TimeoutException("reading/writing from socket timed out");
                }

                return await reading;
            }
        }

        protected async Task<string> Call(string json)
        {
            try
            {
                return await CallImpl(json);
            }
            catch (SocketException ex)
            {
                throw new CommunicationUnsuccessfulException(ex.Message, ex);
            }
            catch (TimeoutException ex)
            {
                throw new CommunicationUnsuccessfulException(ex.Message, ex);
            }
        }


        private static async Task<string> ReadFromPipeAsync (PipeReader reader) {
            var strResult = "";
            while (true) {
                var result = await reader.ReadAsync ();
                var buffer = result.Buffer;

                var content = UTF8String (buffer);
                strResult += content;
                reader.AdvanceTo (buffer.End, buffer.End);

                if (result.IsCompleted) break;
                if (strResult.EndsWith("}\n")) break;
            }

            reader.Complete();
            return strResult;
        }

        private static string UTF8String (ReadOnlySequence<byte> buffer) {
            var result = "";
            foreach (ReadOnlyMemory<byte> segment in buffer)
            {
#if NETCOREAPP2_1
                result += Encoding.UTF8.GetString (segment.Span);
#else
                result += Encoding.UTF8.GetString (segment);
#endif
            }
            return result;
        }
    }
}


#if NET461

internal static class Extensions
{
    public static Task<int> ReceiveAsync(this Socket socket, Memory<byte> memory, SocketFlags socketFlags)
    {
        var arraySegment = GetArray(memory);
        return SocketTaskExtensions.ReceiveAsync(socket, arraySegment, socketFlags);
    }

    public static string GetString(this Encoding encoding, ReadOnlyMemory<byte> memory)
    {
        var arraySegment = GetArray(memory);
        return encoding.GetString(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
    }

    private static ArraySegment<byte> GetArray(Memory<byte> memory)
    {
        return GetArray((ReadOnlyMemory<byte>)memory);
    }

    private static ArraySegment<byte> GetArray(ReadOnlyMemory<byte> memory)
    {
        if (!MemoryMarshal.TryGetArray(memory, out var result))
        {
            throw new InvalidOperationException("Buffer backed by array was expected");
        }

        return result;
    }
}

#endif
